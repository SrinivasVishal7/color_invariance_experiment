import argparse, os
import numpy as np
import cv2


def hist_match(source, template):
    oldshape = source.shape
    source = source.ravel()
    template = template.ravel()
    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True,
                                            return_counts=True)
    print("S Value : {}".format(s_values))
    print("\nBin index : {}".format(bin_idx))
    print("\nS_counts : {}".format(s_counts))
    t_values, t_counts = np.unique(template, return_counts=True)
    s_quantiles = np.cumsum(s_counts).astype(np.float64)
    s_quantiles /= s_quantiles[-1]
    t_quantiles = np.cumsum(t_counts).astype(np.float64)
    t_quantiles /= t_quantiles[-1]

    print("\nS_Quantiles : {}".format(s_quantiles))
    print("\nt_Quantiles : {}".format(t_quantiles))

    interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

    print("\ninterpolated Values : {}".format(interp_t_values))

    return interp_t_values[bin_idx].reshape(oldshape)

def hist_match_rioHist(source, reference):
    orig_shape = source.shape
    source = source.ravel()

    if np.ma.is_masked(reference):
        reference = reference.compressed()
    else:
        reference = reference.ravel()

    s_values, s_idx, s_counts = np.unique(
        source, return_inverse=True, return_counts=True)
    r_values, r_counts = np.unique(reference, return_counts=True)
    s_size = source.size

    if np.ma.is_masked(source):
        mask_index = np.ma.where(s_values.mask)
        s_size = np.ma.where(s_idx != mask_index[0])[0].size
        s_values = s_values.compressed()
        s_counts = np.delete(s_counts, mask_index)

    s_quantiles = np.cumsum(s_counts).astype(np.float64) / s_size
    r_quantiles = np.cumsum(r_counts).astype(np.float64) / reference.size

    interp_r_values = np.interp(s_quantiles, r_quantiles, r_values)

    if np.ma.is_masked(source):
        interp_r_values = np.insert(interp_r_values, mask_index[0], source.fill_value)

    target = interp_r_values[s_idx]

    if np.ma.is_masked(source):
        target = np.ma.masked_where(s_idx == mask_index[0], target)
        target.fill_value = source.fill_value

    return target.reshape(orig_shape)


parser = argparse.ArgumentParser()
parser.add_argument('--template_img', required=True)
parser.add_argument('--source_img', required=True)
parser.add_argument('--if_RGB', default=0)
args = parser.parse_args()

template_img_original = cv2.imread(args.template_img)
source_img_original = cv2.imread(args.source_img)
out_img = np.zeros_like(source_img_original)

if args.if_RGB == '1':
    #for c in range(3):
    #    out_img[:, :, c] = hist_match_rioHist(source_img_original[:, :, c], template_img_original[:, :, c])

    out_img = hist_match_rioHist(source_img_original, template_img_original)

else:

        #template_img = cv2.cvtColor(template_img_original, cv2.COLOR_BGR2YCrCb)
        template_img = cv2.cvtColor(template_img_original, cv2.COLOR_BGR2Lab)
        #source_img = cv2.cvtColor(source_img_original, cv2.COLOR_BGR2YCrCb)
        source_img = cv2.cvtColor(source_img_original, cv2.COLOR_BGR2Lab)

        template_img_light, template_img_cr, template_img_cb = cv2.split(template_img)
        source_img_light, source_img_cr, source_img_cb = cv2.split(source_img)

        out_img[:, :, 0] = hist_match_rioHist(source_img_light, template_img_light)
        out_img[:, :, 1] = source_img_cr
        out_img[:, :, 2] = source_img_cb

        #out_img = cv2.cvtColor(out_img, cv2.COLOR_YCrCb2BGR)
        out_img = cv2.cvtColor(out_img, cv2.COLOR_Lab2BGR)

GRAYFrame = cv2.cvtColor(source_img_original, cv2.COLOR_BGR2GRAY)
ret, thresholded_frame = cv2.threshold(GRAYFrame, 20, 255, cv2.THRESH_BINARY)
#kernel = np.ones((3, 3), np.uint8)
#opening = cv2.morphologyEx(thresholded_frame, cv2.MORPH_OPEN, kernel)
#closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
res = cv2.bitwise_and(out_img, out_img, mask=thresholded_frame)
cv2.imshow("threshold", thresholded_frame)
cv2.imshow("Images", res)
cv2.waitKey(0)
